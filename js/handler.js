document.addEventListener("DOMContentLoaded", Main);

function Main () {

	function Rand (min, max) {
		// diapason: [min; max]
		max++;
		return Math.floor(Math.random() * (max - min)) + min;
	}

	let screenSizes = {
		width: document.body.offsetWidth,
		height: document.documentElement.clientHeight
	};

	class Car {

		constructor () {
			this.htmlCar;
			this.carOffset = 5;	// interval between car and obstacle (free space)
			this.moveStep = 1;
			this.obstacles;	// array obstacles
			this.counterMove = 0;
		}

		get HtmlCar () {
			return this.htmlCar;
		}

		set HtmlCar (htmlCar) {
			this.htmlCar = htmlCar;
		}

		Create () {

			this.htmlCar = document.createElement("div");
			this.htmlCar.classList.add("car");
			
			document.body.appendChild(this.htmlCar);

			// set car position
			let topBorder = screenSizes.height - this.htmlCar.offsetHeight;
			let topPos = Rand(0, topBorder);

			this.htmlCar.style.top = topPos + "px";

			return this.htmlCar;

		}

		GetCurrPos () {
			return this.htmlCar.getBoundingClientRect();
		}

		GetVerticalPosCarToObstacle (obstacle) {
			
			let posObstacle = obstacle.getBoundingClientRect();
			
			if (this.GetCurrPos().top - this.carOffset >= posObstacle.bottom ||
				this.GetCurrPos().bottom + this.carOffset <= posObstacle.top) {
				// car not entry in obstacle
				return  false;
			}
			// car entry in obstacle
			return true;

		}

		GetVerticalPathToScreen (obstacle) {
			
			// determine vertical path car relative to screen borders (top and bottom sides)

			let posObstacle = obstacle.getBoundingClientRect();
			let minHeightPath = parseInt(getComputedStyle(this.htmlCar).height) + (2 * this.carOffset);

			if (posObstacle.top >= minHeightPath && posObstacle.bottom <= screenSizes.height - minHeightPath) {
				return "twoSides";
			}
			else if (posObstacle.top >= minHeightPath) {
				return "up";
			}
			else if (posObstacle.bottom <= screenSizes.height - minHeightPath) {
				return "down";
			}

		}

		GetObstaclesOnVerticalPathToCar () {

			let arrObstacles = [];

			for (let i = 0; i < this.obstacles.length; i++) {

				let currObstaclePos = this.obstacles[i].getBoundingClientRect();
				
				if (!(this.GetCurrPos().left - this.carOffset >= currObstaclePos.right ||
					this.GetCurrPos().right + this.carOffset <= currObstaclePos.left)) {
					arrObstacles.push(this.obstacles[i]);
				}

			}

			return arrObstacles;

		}

		GetObstaclesFromUp () {

			let arrObstacles = this.GetObstaclesOnVerticalPathToCar();
			let newArrObstacles = [];

			let vertcialCenterCar = parseInt(getComputedStyle(this.htmlCar).height) / 2;
			let middleLineCar = this.GetCurrPos().top + vertcialCenterCar;

			for (let i = 0; i < arrObstacles.length; i++) {

				let vertcialCenterObstacle = parseInt(getComputedStyle(arrObstacles[i]).height) / 2;
				let middleLineObstacle = arrObstacles[i].getBoundingClientRect().top + vertcialCenterObstacle;

				if (middleLineObstacle <= middleLineCar) {
					newArrObstacles.push(arrObstacles[i]);
				}

			}

			return newArrObstacles;

		}

		GetObstaclesFromDown () {

			let arrObstacles = this.GetObstaclesOnVerticalPathToCar();
			let newArrObstacles = [];

			let vertcialCenterCar = parseInt(getComputedStyle(this.htmlCar).height) / 2;
			let middleLineCar = this.GetCurrPos().top + vertcialCenterCar;

			for (let i = 0; i < arrObstacles.length; i++) {

				let vertcialCenterObstacle = parseInt(getComputedStyle(arrObstacles[i]).height) / 2;
				let middleLineObstacle = arrObstacles[i].getBoundingClientRect().top + vertcialCenterObstacle;

				if (middleLineObstacle > middleLineCar) {
					newArrObstacles.push(arrObstacles[i]);
				}

			}

			return newArrObstacles;

		}

		GetPathUpToObstacle (arrObstacles, obstacle) {

			// find closest upper obstacle

			if (!arrObstacles.length) {
				return "up";
			}

			let closestUpper = arrObstacles.sort(function (a, b) {
				return b.getBoundingClientRect().bottom - a.getBoundingClientRect().bottom;	// sort by desc
			})[0];

			let minHeightBypass = 
				closestUpper.getBoundingClientRect().bottom + 
				this.GetCurrPos().bottom + this.carOffset - obstacle.getBoundingClientRect().top;

			if (this.GetCurrPos().top - this.carOffset >= minHeightBypass) {
				return "up";
			}

			// no move
			return false;
			
		}

		GetPathDownToObstacle (arrObstacles, obstacle) {

			// find closest lower obstacle

			if (!arrObstacles.length) {
				return "down";
			}

			let closestLower = arrObstacles.sort(function (a, b) {
				return a.getBoundingClientRect().top - b.getBoundingClientRect().top;	// sort by asc
			})[0];

			let minHeightBypass = 
				closestLower.getBoundingClientRect().top - 
				(obstacle.getBoundingClientRect().bottom - (this.GetCurrPos().top - this.carOffset));

			if (this.GetCurrPos().bottom + this.carOffset <= minHeightBypass) {
				return "down";
			}

			// no move
			return false;

		}

		GetVerticalPathToObstacles (obstacle) {

			if (!this.GetObstaclesOnVerticalPathToCar().length) {
				return this.GetVerticalPathToScreen(obstacle);
			}

			switch (this.GetVerticalPathToScreen(obstacle)) {
				case "twoSides":
					
					let closestUpper, closestLower;

					closestUpper = this.GetPathUpToObstacle(this.GetObstaclesFromUp(), obstacle);
					closestLower = this.GetPathDownToObstacle(this.GetObstaclesFromDown(), obstacle);

					if (closestUpper && closestLower) {
						return "twoSides";
					}
					else if (closestUpper) {
						return "up";
					}
					else if (closestLower) {
						return "down";
					}
					else {
						return false;
					}

					break;
				case "up":
					return this.GetPathUpToObstacle(this.GetObstaclesFromUp(), obstacle);
					break;
				case "down":
					return this.GetPathDownToObstacle(this.GetObstaclesFromDown(), obstacle);
					break;
				default:
					// 
					break;
			}

		}

		GetShortPathBypass (obstacle) {

			let posObstacle = obstacle.getBoundingClientRect();

			let clientCenterCar = this.GetCurrPos().top + (parseInt(getComputedStyle(this.htmlCar).height) / 2);

			let distanceUp = Math.abs(clientCenterCar - posObstacle.top);
			let distanceDown = Math.abs(clientCenterCar - posObstacle.bottom);

			if (distanceUp < distanceDown) {
				return "up";
			} 
			else if (distanceDown < distanceUp) {
				return "down";
			}
			else {
				/*
					distanceUp == distanceDown

					method 1:
					checking next obstacle for determine short path
					if next path short up - choice direction up, else - down

					warning:
					possibility loop, where all next obstacle have this case

					method 2:
					set random direction
				*/

				return (Rand(0, 1) ? "up" : "down");
			}

		}

		GetPathBypass (obstacle) {
			
			if (this.GetVerticalPathToObstacles(obstacle) === "twoSides") {
				return this.GetShortPathBypass(obstacle);
			} else {
				return this.GetVerticalPathToObstacles(obstacle);
			}

		}

		SetHorizontalStep (obstacle) {

			let obstacleLeftPos = parseInt(getComputedStyle(obstacle).left);
			obstacle.style.backgroundColor = "red";	// mark closest horizontal obstacle

			requestAnimationFrame(function SetHorizontalStepMove () {

				if ((this.GetCurrPos().right + this.carOffset) < obstacleLeftPos) {
					
					// move to obstacle

					this.htmlCar.style.left = (this.GetCurrPos().left + this.moveStep) + "px";
					requestAnimationFrame(SetHorizontalStepMove.bind(this));

				} else {
					this.SetVerticalStep(obstacle);
				}

			}.bind(this));

		}

		SetVerticalStep (obstacle) {
			
			let step;
			
			if (this.GetVerticalPosCarToObstacle(obstacle)) {

				if (!this.GetPathBypass(obstacle)) {
					return;
				}

				step = this.GetPathBypass(obstacle) === "up" ? -this.moveStep : this.moveStep;

			}

			requestAnimationFrame(function SetVerticalStepMove () {
				
				if (this.GetVerticalPosCarToObstacle(obstacle)) {
					
					// bypass obstacle

					this.htmlCar.style.top = (this.GetCurrPos().top + step) + "px";
					requestAnimationFrame(SetVerticalStepMove.bind(this));

				} else {

					// move to next obstacle
					obstacle.style.backgroundColor = "";
					this.counterMove++;

					if (this.counterMove < this.obstacles.length) {
						this.SetHorizontalStep(this.obstacles[this.counterMove]);
					} else {

						// move to end screen

						requestAnimationFrame(function SetLastStepMove () {
							
							if (this.GetCurrPos().right < screenSizes.width) {

								this.htmlCar.style.left = (this.GetCurrPos().left + this.moveStep) + "px";
								requestAnimationFrame(SetLastStepMove.bind(this));

							}

						}.bind(this));
					}

				}

			}.bind(this));

		}

		Move (obstacles) {

			this.obstacles = obstacles;
			this.SetHorizontalStep(this.obstacles[this.counterMove]);
			
		}

	};

	class Obstacle {

		constructor () {
			this.obstaclesArea = {};
			this.obstacles = [];
		}

		get ObstaclesArea () {
			return this.obstaclesArea;
		}

		set ObstaclesArea (obstaclesArea) {
			// 
		}

		get Obstacles () {
			return this.obstacles;
		}

		set Obstacles (obstacles) {
			// 
		}

		SetObstaclesArea (htmlCar, areaOffset) {

			let carWidth = parseInt(getComputedStyle(htmlCar).width);
			let carHeight = parseInt(getComputedStyle(htmlCar).height);

			this.obstaclesArea = {
				topBorder: 0,
				bottomBorder: screenSizes.height,
				leftBorder: carWidth + areaOffset,
				rightBorder: screenSizes.width
			};

			return this.obstaclesArea;

		}

		SetStyleObstacle (obstacle) {

			let width = Rand(10, 100);
			let height = Rand(10, 100);
			let top = Rand(this.obstaclesArea.topBorder, this.obstaclesArea.bottomBorder - height);
			let left = Rand(this.obstaclesArea.leftBorder, this.obstaclesArea.rightBorder - width);

			obstacle.style.cssText = `
				width: ${width}px;
				height: ${height}px;
				top: ${top}px;
				left: ${left}px;
			`;

		}

		Create (numberObstacle) {

			numberObstacle = numberObstacle || 1;

			if (numberObstacle < 1) {
				throw new RangeError("count obstacles must be > 0");
			}

			for (let i = 0; i < numberObstacle; i++) {

				let obstacle = document.createElement("div");
				obstacle.classList.add("obstacle");

				this.SetStyleObstacle(obstacle);

				document.body.appendChild(obstacle);

				this.obstacles.push(obstacle);

			}

			// sort by closest obstacle to car (left side)
			this.obstacles.sort(function (a, b) {
				return a.getBoundingClientRect().left - b.getBoundingClientRect().left;
			});

			return this.obstacles;

		}

	};

	let car = new Car;
	car.Create();

	let obstacle = new Obstacle;
	obstacle.SetObstaclesArea(car.HtmlCar, 100);
	obstacle.Create(20);
	
	car.Move(obstacle.Obstacles);
}
